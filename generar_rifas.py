#!/usr/bin/env python3
"""Permite generar un archivo PDF conteniendo rifas seriadas

Utiliza como input un template en SVG que puede ser realizado con `inkscape`
o cualquier otro programa de dibujo vectorial. El numero de rifa en el template
debe ser indicado como *placeholder* del estilo ``{0}``, ``{1}``, ``{2}``,
etc... Esos *placeholder* seran reemplazados por el numero de rifa


Uso
~~~~~~~~~~~~~~~~~~

Genera un archivo `output.pdf` en el directorio actual que contiene 100
rifas (desde 0 a 99)

```sh
$ python generar_rifas.py templates/rifa.svg
```

Puede consultar la ayuda del programa para parametrizar la generacion del
PDF destino

```sh
$ python generar_rifas.py -h
usage: generar_rifas.py [-h] [-o OUTPUT] [-m MIN] [-M MAX] [-p POR_PAGINA]
                        template

positional arguments:
  template              Archivo SVG que se usara como plantilla para crear
                        todas las rifas

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        Archivo PDF resultante con todas las rifas seriadas
  -m MIN, --min MIN     Numero desde el cual se generaran las rifas
  -M MAX, --max MAX     Numero hasta el cual se generaran las rifas (no
                        incluido)
  -p POR_PAGINA, --por-pagina POR_PAGINA
                        Cantidad de rifas por pagina en el template
```
"""
import collections
import itertools
import logging
import os
import subprocess
import tempfile


# Logging configuration
LOG_FORMAT = "%(asctime)s %(levelname)-5.5s: %(message)s"  # noqa
logging.basicConfig(format=LOG_FORMAT)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def generar_series(desde, hasta, rifas_por_pagina=5, fmt="{:03d}"):
    """Genera tuplas con las series para cada pagina del pdf.

    Argumentos
    ~~~~~~~~~~~~~~~~~~~~~
    desde: Entero que indica cual sea el numero mas bajo de la serie
    hasta: Entero que indica cual sera el maximo numero de la serie (excluido)
    rifas_por_pagina: Indica cuantas rifas tiene el template por pagina
    fmt: string con el formato que se mostrara el numero de rifa
    """
    for base in range(desde, hasta, rifas_por_pagina):
        yield tuple(fmt.format(x)
                    for x in range(base, base + rifas_por_pagina))


def generar_pdf_serie(template, serie, destino):
    """Genera un archivo PDF con la serie pasada por parametro.

    Utilizando el template, genera una pagina del PDF con reemplazando los
    placeholders por el numero de serie de cada rifa.

    Argumentos
    ~~~~~~~~~~~~~~~~~~~~
    template: string que contiene el template SVG a reemplazar.
    serie: iterable con los numeros de serie a poner en cada rifa del template
    destino: string con el nombre del archivo que se va a generar.
    """
    logger.info("Generando serie %r", serie)
    svg = template.format(*serie)
    logger.info("Generando pagina %r", destino)
    # genero un pdf por cada serie
    convertir_svg_a_pdf(svg, destino)


def convertir_svg_a_pdf(svg, destino):
    """Convierte un archivo SVG a PDF.

    Utilizando ``inkscape``, convierte el contenido SVG (alojado en memoria) en
    un archivo PDF

    Argumentos
    ~~~~~~~~~~~~~~~~~~~~
    svg: String con el contenido del archivo SVG que sera guardado como PDF
    destino: String con el nombre del archivo destino
    """
    # Escribimos el template en un svg temporal y generamos el pdf
    with tempfile.NamedTemporaryFile("wt", suffix=".svg") as f:
        logger.info("Escribiendo svg temporal en el archivo %r", f.name)
        f.write(svg)
        # Convierte a PDF usando inkscape
        cmd = ("inkscape", "--without-gui", f"--export-pdf={destino}", f.name)
        logger.info("Convirtiendo SVG a PDF mediante %r", " ".join(cmd))
        subprocess.run(cmd)


def unir_pdf(paginas, destino):
    """Une todas las paginas en un unico archivo PDF

    Requiere ``pdfunite`` instalado en el host.

    Argumentos
    ~~~~~~~~~~~~~~~
    paginas: Iterable de paths de paginas a incluir
    destino: String con el nombre del archivo a generar
    """
    cmd = tuple(itertools.chain(("pdfunite",), paginas, (destino,)))
    logger.info("Uniendo todos los PDF en un solo mediante: %r", " ".join(cmd))
    subprocess.run(cmd)


def generar_rifas(template,
                  output="output.pdf",
                  desde=0,
                  hasta=100,
                  rifas_por_pagina=5):
    """Genera un PDF con todas las rifas seriadas.

    Necesita un template para generar los PDF. Asume que existen los
    placeholder ``{0}``, ``{1}``, ``{2}``, etc. donde se colocara el numero de
    serie de cada rifa.

    Argumentos
    ~~~~~~~~~~~~~~~~~~~~
    template: string que contiene el template SVG
    output: string con el nombre del archivo a generar
    desde: entero primer numero que se va a generar
    hasta: entero ultimo numero de la serie (no incluido)
    rifas_por_pagina: Indica la cantidad de rifas por pagina en el template
    """
    with tempfile.TemporaryDirectory() as tempdir:
        # almaceno las paginas creadas por cada serie
        paginas = collections.deque()
        # instancio un generador de series por pagina
        generador_series = generar_series(desde, hasta, rifas_por_pagina)
        for pagina, serie in enumerate(generador_series):
            pagina = os.path.join(tempdir, f"{pagina:03}.pdf")
            generar_pdf_serie(template, serie, pagina)
            paginas.append(pagina)
        logger.info("Se han generado %d paginas", len(paginas))
        # uno todos las paginas del PDF en una sola
        unir_pdf(paginas, output)
        logger.info("EXITO! Se ha generado exitosamente el archivo %r", output)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("template", help="""Archivo SVG que se usara como
    plantilla para crear todas las rifas""", type=open)
    parser.add_argument("-o", "--output", default="output.pdf",
                        help="""Archivo PDF resultante con todas las rifas
                          seriadas""")
    parser.add_argument("-m", "--min", default=0, type=int,
                        help="Numero desde el cual se generaran las rifas")
    parser.add_argument("-M", "--max", default=100, type=int,
                        help="""Numero hasta el cual se generaran las rifas (no
                        incluido)""")
    parser.add_argument("-p", "--por-pagina", default=5, type=int,
                        help="Cantidad de rifas por pagina en el template")

    args = parser.parse_args()
    template = args.template.read()
    args.template.close()
    generar_rifas(
        template=template,
        output=args.output,
        desde=args.min,
        hasta=args.max,
        rifas_por_pagina=args.por_pagina,
    )
