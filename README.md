Rifas
===============================

> Permite generar un archivo PDF conteniendo rifas seriadas

Utiliza como input un template en SVG que puede ser realizado con `inkscape`
o cualquier otro programa de dibujo vectorial. El numero de rifa en el template
debe ser indicado como *placeholder* del estilo ``{0}``, ``{1}``, ``{2}``,
etc... Esos *placeholder* seran reemplazados por el numero de rifa


Requisitos
-----------

* Python 3.6+
* [Inkscape](https://inkscape.org/)
* [pdfunite](https://github.com/mtgrosser/pdfunite)

*No requiere la instaacion de bibliotecas python extras, solo las built-in*

Uso
----------

Genera un archivo `rifa-dia-del-padre.pdf` que contiene 100 rifas numeradas
(desde 0 a 99)

```sh
$ python3 generar_rifas.py examples/rifa-dia-del-padre.svg -o examples/rifa-dia-del-padre.pdf
```

El archivo generado por defecto se llama `output.pdf`

Puede consultar la ayuda del programa para parametrizar la generacion del
PDF destino

```sh
$ python3 generar_rifas.py -h
usage: generar_rifas.py [-h] [-o OUTPUT] [-m MIN] [-M MAX] [-p POR_PAGINA]
                        template

positional arguments:
  template              Archivo SVG que se usara como plantilla para crear
                        todas las rifas

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        Archivo PDF resultante con todas las rifas seriadas
  -m MIN, --min MIN     Numero desde el cual se generaran las rifas
  -M MAX, --max MAX     Numero hasta el cual se generaran las rifas (no
                        incluido)
  -p POR_PAGINA, --por-pagina POR_PAGINA
                        Cantidad de rifas por pagina en el template
```
